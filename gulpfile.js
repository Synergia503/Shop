var gulp = require('gulp');
var gulpSass = require('gulp-sass');
var wait = require('gulp-wait');

gulp.task('buildcss', () => {
    return gulp.src('./dev-assets/style.scss')
        .pipe(wait(1000))
        .pipe(gulpSass())
        .pipe(gulp.dest('./prod-assets'));
});

gulp.task('watch', () => {
    gulp.watch('./dev-assets/**/*.scss', ['buildcss']);
});